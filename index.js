const packageName = () => {
    return "My Awesome Package";
  };
  const greet = () => {
    console.log("Hello!");
  };
  
  module.exports = {
    packageName: packageName,
    greet: greet,
  };